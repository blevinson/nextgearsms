# NextGear Student Management System

#Overview

The purpose of this assessment is to have you build a simple “CRUD” application that you will be asked to bring with you to the interview.  At the interview, in addition to other behavioral and cultural fit questions, you will be asked to walk a small group of engineers through the work you did in this assessment.  We recommend you bring your source code and any other supporting materials on a thumb drive so that they can be displayed on a screen for the group to review.

#Requirements

For the purpose of this assessment, the application you will build will be very simple.  It will be a Student Management System that has the following features:

1.	When first launched, the application should display a list of all students in the database (no paging is necessary).
2.	A student on the list can be deleted by the end user.  When the student is being deleted, the user should be presented with an “are you sure” dialog box.
3.	A student on the list can be updated by the end user.
4.	A new student can be added to the list by the end user.

#Architecture

The intent of this assessment is to conceptually match the way that we build software at NextGear Capital.  Therefore, the application must store its data in a relational database.  The business logic must be written in an application server that communicates with the database and is accessed by the user interface via a REST API.  The user interface must be HTML/Javascript and be completely decoupled from the middle tier, accessing it only through the REST API and not accessing the database directly.

This uses Spring Boot for the backend/middle tier connected to a postgres database. The front end is ReactJS/CSS/HTML.

#Getting Started

1. Clone repository.
2. Create a postgres database.
3. Add database info to application.properties
4. CD to cloned directory.
5. Run: mvn spring-boot:run
