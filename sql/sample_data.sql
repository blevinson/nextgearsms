--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: Brant
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO "Brant";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: student; Type: TABLE; Schema: public; Owner: Brant
--

CREATE TABLE public.student (
    id bigint NOT NULL,
    description character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    version bigint,
    grade character varying(255)
);


ALTER TABLE public.student OWNER TO "Brant";

--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: Brant
--

COPY public.student (id, description, first_name, last_name, version, grade) FROM stdin;
30	\N	John	Thompson	0	Senior
27	\N	Tommy	Jones	2	Sophmore
35	\N	John	Doe	0	Senior
26	\N	Jim	Baggins	2	Freshmen
29	\N	Meria	Bucks	2	Freshmen
28	\N	Sam	Gagle	2	Junior
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: Brant
--

SELECT pg_catalog.setval('public.hibernate_sequence', 35, true);


--
-- Name: student student_pkey; Type: CONSTRAINT; Schema: public; Owner: Brant
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

