
package com.nextgear.sms;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class Student {

	private @Id @GeneratedValue Long id;
	private String firstName;
	private String lastName;
	private String grade;

	private @Version @JsonIgnore Long version;

	private Student() {}

	public Student(String firstName, String lastName, String grade) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.grade = grade;
	}
}
