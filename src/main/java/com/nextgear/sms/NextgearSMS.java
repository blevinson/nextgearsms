
package com.nextgear.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class NextgearSMS {

	public static void main(String[] args) {
		SpringApplication.run(NextgearSMS.class, args);
	}
}