'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const when = require('when');
const client = require('./client');

const follow = require('./follow');

const root = '/api';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {students: [], attributes: [], links: {}};
		this.onCreate = this.onCreate.bind(this);
		this.onUpdate = this.onUpdate.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
	}

	loadFromServer() {
		follow(client, root, [
			{rel: 'students'}]
		).then(studentCollection => {
			return client({
				method: 'GET',
				path: studentCollection.entity._links.profile.href,
				headers: {'Accept': 'application/schema+json'}
			}).then(schema => {
				this.schema = schema.entity;
				this.links = studentCollection.entity._links;
				return studentCollection;
			});
		}).then(studentCollection => {
			return studentCollection.entity._embedded.students.map(student =>
					client({
						method: 'GET',
						path: student._links.self.href
					})
			);
		}).then(studentPromises => {
			return when.all(studentPromises);
		}).done(students => {
			this.setState({
				students: students,
				attributes: Object.keys(this.schema.properties),
				links: this.links
			});
		});
	}

	onCreate(newStudent) {
		var self = this;
		follow(client, root, ['students']).then(response => {
			return client({
				method: 'POST',
				path: response.entity._links.self.href,
				entity: newStudent,
				headers: {'Content-Type': 'application/json'}
			})
		}).then(response => {
			return follow(client, root, [{rel: 'students'}]);
		}).done(response => {
			if (typeof response.entity._links.last != "undefined") {
				this.onNavigate(response.entity._links.last.href);
			} else {
				this.onNavigate(response.entity._links.self.href);
			}
		});
	}

	onUpdate(student, updatedStudent) {
		client({
			method: 'PUT',
			path: student.entity._links.self.href,
			entity: updatedStudent,
			headers: {
				'Content-Type': 'application/json',
				'If-Match': student.headers.Etag
			}
		}).done(response => {
			this.loadFromServer();
		}, response => {
			if (response.status.code === 412) {
				alert('DENIED: Unable to update ' +
					student.entity._links.self.href + '. Your copy is stale.');
			}
		});
	}

	onDelete(student) {
		client({method: 'DELETE', path: student.entity._links.self.href}).done(response => {
			this.loadFromServer();
		});
	}

	onNavigate(navUri) {
		client({
			method: 'GET',
			path: navUri
		}).then(studentCollection => {
			this.links = studentCollection.entity._links;

			return studentCollection.entity._embedded.students.map(student =>
					client({
						method: 'GET',
						path: student._links.self.href
					})
			);
		}).then(studentPromises => {
			return when.all(studentPromises);
		}).done(students => {
			this.setState({
				students: students,
				attributes: Object.keys(this.schema.properties),
				links: this.links
			});
		});
	}


	componentDidMount() {
		this.loadFromServer();
	}

	render() {
		return (
			<div>
				<StudentList students={this.state.students}
							  links={this.state.links}
							  attributes={this.state.attributes}
							  onNavigate={this.onNavigate}
							  onUpdate={this.onUpdate}
							  onDelete={this.onDelete}/>
				<CreateDialog id={} attributes={this.state.attributes} onCreate={this.onCreate}/>

			</div>
		)
	}
}

class CreateDialog extends React.Component {

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		var newStudent = {};
		this.props.attributes.forEach(attribute => {
			newStudent[attribute] = ReactDOM.findDOMNode(this.refs[attribute]).value.trim();
		});
		this.props.onCreate(newStudent);
		this.props.attributes.forEach(attribute => {
			ReactDOM.findDOMNode(this.refs[attribute]).value = '';
		});
		window.location = "#";
	}

	render() {
		var inputs = this.props.attributes.map(attribute =>
			<p key={attribute}>
				<input type="text" placeholder={attribute} ref={attribute} className="field" />
			</p>
		);
		return (
			<div>

				<div id="createStudent" className="modalDialog">
					<div>
						<a href="#" title="Close" className="close">X</a>

						<h2>Create New Student</h2>

						<form>
							{inputs}
							<button onClick={this.handleSubmit}>Create</button>
						</form>
					</div>
				</div>
				<a href="#createStudent"><button>Create</button></a>
			</div>
		)
	}
}

class UpdateDialog extends React.Component {

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		var updatedStudent = {};
		this.props.attributes.forEach(attribute => {
			updatedStudent[attribute] = ReactDOM.findDOMNode(this.refs[attribute]).value.trim();
		});
		this.props.onUpdate(this.props.student, updatedStudent);
		window.location = "#";
	}

	render() {
		var inputs = this.props.attributes.map(attribute =>
				<p key={this.props.student.entity[attribute]}>
					<input type="text" placeholder={attribute}
						   defaultValue={this.props.student.entity[attribute]}
						   ref={attribute} className="field" />
				</p>
		);

		var dialogId = "updateStudent-" + this.props.student.entity._links.self.href;

		return (
			<div key={this.props.student.entity._links.self.href}>
				<a href={"#" + dialogId}><button>Update</button></a>
				<div id={dialogId} className="modalDialog">
					<div>
						<a href="#" title="Close" className="close">X</a>

						<h2>Update a Student</h2>

						<form>
							{inputs}
							<button onClick={this.handleSubmit}>Update</button>
						</form>
					</div>
				</div>
			</div>
		)
	}

}

class DeleteDialog extends React.Component {

    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);

    }

    handleDelete(e) {
        e.preventDefault();
        this.props.onDelete(this.props.student);
        window.location = "#";
    }

    render() {

        var dialogId = "deleteStudent-" + this.props.student.entity._links.self.href;

        return (
			<div key={this.props.student.entity._links.self.href}>
				<a href={"#" + dialogId}><button>Delete</button></a>
				<div id={dialogId} className="modalDialog">
					<div>
						<a href="#" title="Close" className="close">X</a>

						<h2>Confirm Delete a Student</h2>
							<button onClick={this.handleDelete}>Delete</button>
					</div>
				</div>
			</div>
        )
    }

}



class StudentList extends React.Component {

	render() {
		var students = this.props.students.map(student =>
				<Student key={student.entity._links.self.href}
						  student={student}
						  attributes={this.props.attributes}
						  onUpdate={this.props.onUpdate}
						  onDelete={this.props.onDelete}/>
		);


		return (
			<div>
				<table>
					<tbody>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Grade</th>
							<th></th>
							<th></th>
						</tr>
						{students}
					</tbody>
				</table>
			</div>
		)
	}
}

class Student extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<tr>
				<td>{this.props.student.entity.firstName}</td>
				<td>{this.props.student.entity.lastName}</td>
				<td>{this.props.student.entity.grade}</td>
				<td>
					<UpdateDialog student={this.props.student}
								  attributes={this.props.attributes}
								  onUpdate={this.props.onUpdate}/>
				</td>
				<td>
					<DeleteDialog student={this.props.student}
								  attributes={this.props.attributes}
								  onDelete={this.props.onDelete}/>
				</td>
			</tr>
		)
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('react')
);
